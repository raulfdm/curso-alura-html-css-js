var $cardList = document.querySelector('.card-list');
var $cardOptions = document.querySelectorAll('.card-options');

$cardList.addEventListener('click', function(event) {
    var $this = event.target;
    var $card = $this.parentNode.parentNode.parentNode;    
    var $cardContent = $card.querySelector('.card-content');

    if ($this.dataset.color) {
        for (var pos = 0; pos < $cardOptions.length; pos++) {
            $cardOptions[pos].classList.remove('ativo');

        }
        $card.dataset.color = $this.dataset.color;
        $this.classList.add('ativo');

    }

    if ($this.classList.contains('card_delete')) {
        $card.remove();
    }


    if ($this.classList.contains('card_edit')) {

        if ($cardContent.getAttribute('contenteditable', 'false') == 'false') {
            $cardContent.setAttribute('contenteditable', 'true');
            $cardContent.focus();
            $this.classList.add('ativo');
        } else {
            $cardContent.setAttribute('contenteditable', 'false');
            $cardContent.blur();
            $this.classList.remove('ativo');
        }

    }
});