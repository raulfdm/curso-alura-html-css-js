var $newCard = document.querySelector('.newCard');
var $newCardContent = document.querySelector('.newCard-content');
var $newCardAction = document.querySelector('.newCard-action');


//Evento de envio do form
$newCard.addEventListener('submit', function(event) {

    event.preventDefault();

    if (!$newCardContent.value) {

        if (!document.querySelector('.error')) {
            var $errorSpan = document.createElement('span');
            $errorSpan.classList.add('error');
            $errorSpan.textContent = 'Por favor, preencha o campo acima.';
            $newCard.insertBefore($errorSpan, $newCardAction);
        }
    } else {
        var $father = document.querySelector('.card-list');
        var $card = document.querySelector('.card');
        var $copyCard = $card.cloneNode(true);

        $copyCard.querySelector('.card-content').textContent = $newCardContent.value;
        $father.insertBefore($copyCard, $card);
        //console.log($copyCard)
    }

});

//Evento para apagar a mensagem quando escrever
$newCardContent.addEventListener('input', function() {
    var $error = document.querySelector('.error');

    if ($error) {
        $error.remove();
    }

});